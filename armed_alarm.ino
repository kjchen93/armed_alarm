#include <LiquidCrystal.h>
#include <Keypad.h>
#include <EEPROM.h>
#define Password_Lenght 5
int sensor;
#define PIR_PIN 5
#define BUZZ_PIN 4
#define LED_PIN 3

//LCD
const int rs = 1, en = 0, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//KEYPAD
const byte ROWS = 4; 
const byte COLS = 4; 
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {9, 8, 7, 6}; 
byte colPins[COLS] = {A1, A2, A3, A4}; 
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
char FistTimePassword[] = {'1','2','3','4'};

//MOTOR 
const int motor_pin1  = 10, motor_pin2=11;


class Keyboard
{
public:
  bool process_char()
  {
    bool result = false;
    char key = keypad.getKey(); 
    if(key){
        Serial.println(key);
    }


    if (key) 
    {
        collectKey(key);
    }

    if(data_count == Password_Lenght-1) 
    {
        if(mode == 0){
        if(!strcmp(Data, Master)) {
            Serial.print("UNLOCKED\nEnter Password to LOCK:\n");
            result = true;
            just_allowed_pass = 1;
            time_old = millis();
            digitalWrite(13, HIGH);
            
            mode = 3;
            clearData();
        }else{
            Serial.print("INCORRECT PASSWORD!\n");
            
        }
        
        clearData();
        }else if( mode == 3){ 
        if(!strcmp(Data, Master)) {
            Serial.print("LOCKED\nEnter Password to UNLOCK:\n");
            result = true;
            just_allowed_pass = 1;
            time_old = millis();
            digitalWrite(13, HIGH);
            
            mode = 0;
            clearData();
        }else{
            Serial.print("INCORRECT PASSWORD!\n");
            
        }
        
        clearData();
        }  
    }
    return result;  
  }
  void collectKey(char key){
    Data[data_count] = key; 
    data_count++;   
  }

  void clearData()
  {
    while(data_count !=0)
    {  
      Data[data_count--] = 0; 
    }
  }


void Check_EEPROM(){
  EEPROM.get(0, Master);
  if(Master[0] == 0 && Master[1] == 0 && Master[2] == 0 && Master[3] == 0){ // check if EEPRM have store password ?
    Serial.println("Enter password to UNLOCK:\n"); // if not found will burn EEPROM a first time password
    EEPROM.put(0, FistTimePassword);
    EEPROM.get(0, Master);
  }
}

private:
  byte data_count = 0; 
  byte mode = 0;
  char Data[Password_Lenght]; 
  char Master[Password_Lenght];
  bool just_allowed_pass = 0;
  long time_old = 0;

};



Keyboard keyboard;
bool door = false;


void flipDoor()
{
digitalWrite(motor_pin1,door);
digitalWrite(motor_pin2,!door);
door=!door;
delay(2000);
 digitalWrite(motor_pin1,LOW);
 digitalWrite(motor_pin2,LOW);
}


void setup()
{
  //BUZZ
  pinMode(BUZZ_PIN, OUTPUT);
  pinMode(PIR_PIN, INPUT);
  noTone(BUZZ_PIN);
  
  //LED  
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  
  keyboard.Check_EEPROM();
  
  //Motor setup
  pinMode(motor_pin1,OUTPUT);
  pinMode(motor_pin2,OUTPUT);
  
  //Serial
  Serial.begin(9600);
}



void loop()
{
  bool flip = keyboard.process_char();
  if(flip == true)
    flipDoor();
  
  sensor = digitalRead(PIR_PIN);
  if(sensor > 0 && !door){
  	tone(BUZZ_PIN,132);
    digitalWrite(LED_PIN, HIGH);
    delay(500);
    noTone(BUZZ_PIN);
    digitalWrite(LED_PIN, LOW);
  } 
}