# ArmedAlarm

A tinkercad designed program for an alarm system with a PIR sensor. A motor is embedded to the system to simulate a door. When the door is locked, the alarm system becomes armed. The alarm is disarmed when the door is unlocked.

![](./imgs/armed_alarm.png)

The project can be copied and modified through: https://www.tinkercad.com/things/ckjSqsmLQLD


## Authors

- *Ole Kjepso*
- *Kai Chen*
